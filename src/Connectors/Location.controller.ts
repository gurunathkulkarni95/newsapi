import { Request, Response } from "express";
import {
  PARAMS_ERROR_MESSAGE,
  SUCCESS_DELETE_ERROR_MESSAGE,
  SUCCESS_DELETE_MESSAGE,
} from "../utils/constants";
import {
  activeLocation,
  createLocation,
  deleteLocation,
  editLocationService,
  GetLocation,
  inActiveLocation,
} from "../Services/Location.service";
import {
  BODY_ERROR_MESSAGE,
  SOME_THING_WENT_WRONG,
  STATUS_CODES,
  SUCCESS_MESSAGE,
  SUCCESS_UPDATE_MESSAGE,
} from "../utils/constants";

export async function createLocationHandler(req: Request, res: Response) {
  try {
    const location = await createLocation(req.body);
    if (location.location) {
      return res.send({
        status_code: STATUS_CODES.SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        location,
      });
    } else {
      return res.send({
        status_code: STATUS_CODES.ERROR_CODE,
        message: location.errorMessage,
        location,
      });
    }
  } catch (e: any) {
    console.error(e);
    return res.status(STATUS_CODES.ERROR_CODE).send(e.message);
  }
}

export async function editLocation(req: Request, res: Response) {
  try {
    if (!req.body) {
      res
        .status(STATUS_CODES.ERROR_CODE)
        .send({ errorMessage: BODY_ERROR_MESSAGE });
    }
    const editLocation = await editLocationService(req.body);
    if (editLocation?.errorMessage) {
      return res.send({ message: "error in edit", error: editLocation });
    }
    res.send({
      message: SUCCESS_UPDATE_MESSAGE,
      status_code: STATUS_CODES.SUCCESS_CODE,
    });
  } catch (e: any) {
    console.error(e);
    return res.status(STATUS_CODES.ERROR_CODE).send(e.message);
  }
}

export async function listLocation(req: Request, res: Response) {
  try {
    const listLocation = await GetLocation();
    res.send(listLocation);
  } catch (e: any) {
    console.error(e);
    return res.status(STATUS_CODES.ERROR_CODE).send(e.message);
  }
}

export async function deleteLocationController(req: Request, res: Response) {
  try {
    if (!req.params.id) {
      return res
        .status(STATUS_CODES.ERROR_CODE)
        .send({ errorMessage: PARAMS_ERROR_MESSAGE });
    }
    const result = await deleteLocation(req.params.id);
    console.log(result);
    if (!result || result === undefined) {
      return res.send({
        message: SUCCESS_DELETE_ERROR_MESSAGE,
        status_code: STATUS_CODES.ERROR_CODE,
      });
    }
    res.send({
      message: SUCCESS_DELETE_MESSAGE,
      status_code: STATUS_CODES.SUCCESS_CODE,
    });
  } catch (e: any) {
    return res.status(STATUS_CODES.ERROR_CODE).send(e.message);
  }
}

export async function activeLocationController(req: Request, res: Response) {
  try {
    if (!req.params.id) {
      return res
        .status(STATUS_CODES.ERROR_CODE)
        .send({ errorMessage: PARAMS_ERROR_MESSAGE });
    }
    const result = await activeLocation(req.params.id);
    res.send(result);
  } catch (e: any) {
    return res.status(STATUS_CODES.ERROR_CODE).send(e.message);
  }
}

export async function inActiveLocationController(req: Request, res: Response) {
  try {
    if (!req.params.id) {
      return res
        .status(STATUS_CODES.ERROR_CODE)
        .send({ errorMessage: PARAMS_ERROR_MESSAGE });
    }
    const result = await inActiveLocation(req.params.id);
    res.send(result);
  } catch (e: any) {
    return res.status(STATUS_CODES.ERROR_CODE).send(e.message);
  }
}

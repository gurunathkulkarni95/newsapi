import { Request, Response } from "express";
import { createUser, LoginUser } from "../Services/User.service";
import { BODY_ERROR_MESSAGE, SOME_THING_WENT_WRONG, STATUS_CODES } from "../utils/constants";

export async function createUserHandler(req: Request, res: Response) {
  try {
    const user = await createUser(req.body);

    return res.send({user});
  } catch (e: any) {
    console.error(e);
    return res.status(STATUS_CODES.ERROR_CODE).send(e.message);
  }
}

export async function loginUserHandler(req: Request, res: Response) {
    try {
        if (!req.body) {
            res.status(STATUS_CODES.ERROR_CODE).json(BODY_ERROR_MESSAGE);
        }
        const userLogin = await LoginUser(req.body);
        res.send(userLogin);
    } catch(e:any){
        res.send(SOME_THING_WENT_WRONG)
    }
}
import { Request, Response } from "express";
import {
  createNewsService,
  createNewsTypesService,
  deleteNewsService,
  deleteNewsTypeService,
  editNewsService,
  editNewsTypeService,
  getNewsService,
  getNewsTypeService,
  makeNewsTypeActive,
  makeNewsTypeInActive,
  newsActiveService,
  newsInActiveService,
} from "../Services/news.service";
import {
  BODY_ERROR_MESSAGE,
  PARAMS_ERROR_MESSAGE,
  SOME_THING_WENT_WRONG,
  STATUS_CODES,
  SUCCESS_DELETE_MESSAGE,
  SUCCESS_MESSAGE,
  UPDATE_MESSAGE,
} from "../utils/constants";

export async function getNewsTypes(req: Request, res: Response) {
  try {
    const newsData = await getNewsTypeService(req.body);
    return res.send(newsData);
  } catch (e: any) {
    console.error(e);
    return res.status(STATUS_CODES.ERROR_CODE).send(e.message);
  }
}

export async function createNewsTypes(req: Request, res: Response) {
  try {
    if (!req.body) {
      res.status(STATUS_CODES.ERROR_CODE).json(BODY_ERROR_MESSAGE);
    }
    const createdNewsTypesData: any = await createNewsTypesService(req.body);
    if (createdNewsTypesData?.errorMessage) {
      return res.send({
        message: "error in adding",
        error: createdNewsTypesData,
        status_code: STATUS_CODES.ERROR_CODE,
      });
    }
    res.send({
      message: SUCCESS_MESSAGE,
      createdNewsTypesData,
      status_code: STATUS_CODES.SUCCESS_CODE,
    });
  } catch (e: any) {
    res.send(SOME_THING_WENT_WRONG);
  }
}

export async function editNewsType(req: Request, res: Response) {
  try {
    if (!req.body) {
      res.status(STATUS_CODES.ERROR_CODE).json(BODY_ERROR_MESSAGE);
    }
    const editedNews: any = await editNewsTypeService(req.body);
    if (editedNews?.errorMessage) {
      return res.send({ message: "error in edit", error: editedNews });
    }
    res.send({ message: UPDATE_MESSAGE });
  } catch (e: any) {
    res.send(SOME_THING_WENT_WRONG);
  }
}

export async function editNews(req: Request, res: Response) {
  try {
    if (!req.body) {
      res.status(STATUS_CODES.ERROR_CODE).json(BODY_ERROR_MESSAGE);
    }
    const editedNews: any = await editNewsService(req.body);
    if (editedNews?.errorMessage) {
      return res.send({ message: "error in edit", error: editedNews });
    }
    res.send({ message: UPDATE_MESSAGE });
  } catch (e: any) {
    res.send(SOME_THING_WENT_WRONG);
  }
}

export async function getNews(req: Request, res: Response) {
  try {
    const newsData = await getNewsService(req.query);
    return res.send(newsData);
  } catch (e: any) {
    console.error(e);
    return res.status(STATUS_CODES.ERROR_CODE).send(e.message);
  }
}

export async function createNews(req: Request, res: Response) {
  try {
    if (!req.body) {
      res.status(STATUS_CODES.ERROR_CODE).json(BODY_ERROR_MESSAGE);
    }
    const createdNewsData: any = await createNewsService(req.body);
    if (createdNewsData?.errorMessage) {
      return res.send({ message: "error in adding", error: createdNewsData });
    }
    res.send({ message: SUCCESS_MESSAGE, createdNewsData });
  } catch (e: any) {
    res.send(SOME_THING_WENT_WRONG);
  }
}

export async function deleteNewsType(req: Request, res: Response) {
  try {
    if (!req.body) {
      res.status(STATUS_CODES.ERROR_CODE).json(BODY_ERROR_MESSAGE);
    }
    const deletedNewsType: any = await deleteNewsTypeService(req.body);
    if (deletedNewsType?.errorMessage) {
      return res.send({ message: "error in deleting", error: deletedNewsType });
    }
    res.send({ message: SUCCESS_DELETE_MESSAGE, deletedNewsType });
  } catch (e: any) {
    res.send(SOME_THING_WENT_WRONG);
  }
}

export async function deleteNews(req: Request, res: Response) {
  try {
    if (!req.params) {
      res.status(STATUS_CODES.ERROR_CODE).json(PARAMS_ERROR_MESSAGE);
    }
    const deletedNews: any = await deleteNewsService(req.params);
    if (deletedNews?.errorMessage) {
      return res.send({ message: "error in deleting", error: deletedNews });
    }
    res.send({ message: SUCCESS_DELETE_MESSAGE, deletedNews });
  } catch (e: any) {
    res.send(SOME_THING_WENT_WRONG);
  }
}

export async function newsActive(req: Request, res: Response) {
  try {
    if (!req.params) {
      res.status(STATUS_CODES.ERROR_CODE).json(PARAMS_ERROR_MESSAGE);
    }
    const result: any = await newsActiveService(req.params.id);
    res.send(result);
  } catch (e: any) {
    res.send(SOME_THING_WENT_WRONG);
  }
}

export async function newsInActive(req: Request, res: Response) {
  try {
    if (!req.params) {
      res.status(STATUS_CODES.ERROR_CODE).json(PARAMS_ERROR_MESSAGE);
    }
    const result: any = await newsInActiveService(req.params.id);
    res.send(result);
  } catch (e: any) {
    res.send(SOME_THING_WENT_WRONG);
  }
}

export async function newsTypeActive(req: Request, res: Response) {
  try {
    if (!req.params) {
      res.status(STATUS_CODES.ERROR_CODE).json(PARAMS_ERROR_MESSAGE);
    }
    const result: any = await makeNewsTypeActive(req.params.id);
    res.send(result);
  } catch (e: any) {
    res.send(SOME_THING_WENT_WRONG);
  }
}

export async function newsTypeInActive(req: Request, res: Response) {
  try {
    if (!req.params) {
      res.status(STATUS_CODES.ERROR_CODE).json(PARAMS_ERROR_MESSAGE);
    }
    const result: any = await makeNewsTypeInActive(req.params.id);
    res.send(result);
  } catch (e: any) {
    res.send(SOME_THING_WENT_WRONG);
  }
}

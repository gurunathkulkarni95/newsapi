export enum STATUS_CODES {
    ERROR_CODE = 400,
    SUCCESS_CODE = 200,
    VAIDATE_STATUS_ERROR = 401
}

export const SUCCESS_MESSAGE = "Successfully Added";
export const SUCCESS_UPDATE_MESSAGE = "Successfully updated";
export const SUCCESS_DELETE_MESSAGE = "Successfully deleted";
export const SUCCESS_DELETE_ERROR_MESSAGE = "Failed to deleted either item is not exists";
export const UPDATE_MESSAGE = "Successfully updated";
export const BODY_ERROR_MESSAGE = {error: "Body Error"};
export const PARAMS_ERROR_MESSAGE = {error: "Params Error"};

export const SOME_THING_WENT_WRONG = { error: 'something went wrong' };
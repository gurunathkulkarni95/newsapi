import express from "express";
import { json } from "body-parser";
import cors from 'cors';

// route imports
import { CategoryRoute } from "./routes/category";
import { userRoute } from "./routes/user.route";
import { NewsRoute } from "./routes/news.route";
import { locationRoute } from "./routes/location.route";
import connect from './Db/index';

const port = 8080;
const app = express();
app.use(cors());
app.use(json());
app.use(CategoryRoute);
app.use(userRoute);
app.use(NewsRoute);
app.use(locationRoute);
connect();

app.listen(port,()=> {
    console.log('server started in '+port);
})
import { DocumentDefinition } from "mongoose";
import User, { UserDocument } from "../Models/user.model";
import { STATUS_CODES } from '../utils/constants';

export async function createUser(input: DocumentDefinition<UserDocument>) {
  try {
    const existingUser = await User.find({ email: input.email });
    if (existingUser.length) {
        return { errorMessage: "User already Exists", errorCode: STATUS_CODES.ERROR_CODE };
    }
    const result = await User.create(input);
    return result;
  } catch (error: any) {
    return { errorMessage: error, errorCode: STATUS_CODES.ERROR_CODE }
  }
}

export async function LoginUser(input: any) {
    const existingUser = await User.find({ email: input.email, password: input.password });
    if (existingUser.length) {
        return { message: "login success", data: existingUser };
    } else {
        return { message: 'login failed . Please check the email and password'}
    }
}
import { DocumentDefinition } from "mongoose";
import NewsModel, { NewsDocument } from "../Models/News.model";
import NewsTypeModel, { NewstypeDocument } from "../Models/NewsType.model";
import { STATUS_CODES } from "../utils/constants";

export async function createNewsService(
  input: DocumentDefinition<NewsDocument>
) {
  try {
    input.isActive = false;
    const result = await NewsModel.create(input);
    return result;
  } catch (error: any) {
    return { errorMessage: error, errorCode: STATUS_CODES.ERROR_CODE };
  }
}

export async function getNewsService(input: any) {
  const { id } = input;
  let existingUser: any;
  if (id) {
    existingUser = await NewsModel.findById(id);
  } else {
    existingUser = await NewsModel.find();
  }
  if (existingUser.length || Object.keys(existingUser).length) {
    return { message: "Data fetched successfully", data: existingUser };
  } else {
    return { message: "No data found" };
  }
}

export async function getNewsTypeService(input: any) {
  const types = await NewsTypeModel.find();
  if (types.length) {
    return { message: "Data fetched successfully", data: types };
  } else {
    return { message: "No data found" };
  }
}

export async function createNewsTypesService(
  input: DocumentDefinition<NewstypeDocument>
) {
  try {
    const existingType = await NewsTypeModel.find({ type: input.type });
    if (existingType.length) {
      return { error: "Data already exists" };
    }
    const result = await NewsTypeModel.create(input);
    return result;
  } catch (error: any) {
    console.log('error', error);
    return { errorMessage: error, status_code: STATUS_CODES.ERROR_CODE };
  }
}

export async function editNewsService(
  input: DocumentDefinition<NewstypeDocument>
) {
  try {
    if (!input._id) {
      return { error: "data not found" };
    }
    delete input._id;
    const updatedNews: any = await NewsModel.updateOne(input._id, input);
    return updatedNews;
  } catch (error: any) {
    return { errorMessage: error, status_code: STATUS_CODES.ERROR_CODE };
  }
}

export async function editNewsTypeService(
  input: DocumentDefinition<NewstypeDocument>
) {
  try {
    if (!input._id) {
      return { error: "data not found" };
    }
    delete input._id;
    const updatedNews: any = await NewsTypeModel.updateOne(input._id, input);
    // console.log('updatedNews', updatedNews);
    return updatedNews;
  } catch (error: any) {
    return { errorMessage: error, status_code: STATUS_CODES.ERROR_CODE };
  }
}

export async function deleteNewsTypeService(input: any) {
  try {
    if (!input.length) {
      return {
        error: "data not found in array. You can pass as [ id, id, id ]",
      };
    }
    const result: any = [];
    input.map(async (item: any) => {
      const deletedNewsType: any = await NewsTypeModel.findByIdAndDelete(item);
      if (deletedNewsType?._id) {
        result.push({
          typeId: deletedNewsType._id,
          type: deletedNewsType.type,
        });
      } else {
        result.push({ typeId: item, message: "failed to delete" });
      }
    });
    return result;
  } catch (error: any) {
    return { errorMessage: error, status_code: STATUS_CODES.ERROR_CODE };
  }
}

export async function makeNewsTypeActive(id: string) {
  try {
    const existingNewsTypes = await NewsTypeModel.findById(id);
    if (!existingNewsTypes) {
      return {
        errorMessage: "No News Found",
        status_code: STATUS_CODES.ERROR_CODE,
      };
    }
    existingNewsTypes.isActive = true;
    existingNewsTypes.save();
    return {
      message: "successfully in-active",
      status_code: STATUS_CODES.SUCCESS_CODE,
    };
  } catch (e: any) {
    return { errorMessage: e, status_code: STATUS_CODES.ERROR_CODE };
  }
}

export async function makeNewsTypeInActive(id: string) {
  try {
    const existingNewsTypes = await NewsTypeModel.findById(id);
    if (!existingNewsTypes) {
      return {
        errorMessage: "No News Found",
        status_code: STATUS_CODES.ERROR_CODE,
      };
    }
    existingNewsTypes.isActive = false;
    existingNewsTypes.save();
    return {
      message: "successfully in-active",
      status_code: STATUS_CODES.SUCCESS_CODE,
    };
  } catch (e: any) {
    return { errorMessage: e, status_code: STATUS_CODES.ERROR_CODE };
  }
}

export async function deleteNewsService(input: any) {
  try {
    const deletedNews: any = await NewsModel.findByIdAndDelete(input.id);
    if (deletedNews === null) {
      return {
        errorMessage:
          "error in deleting " + input.id + " id not found on datastore",
      };
    }
    return deletedNews;
  } catch (error: any) {
    return { errorMessage: error, status_code: STATUS_CODES.ERROR_CODE };
  }
}

export async function newsActiveService(id: string) {
  try {
    const existingNews = await NewsModel.findById(id);
    if (!existingNews) {
      return {
        errorMessage: "No News Found",
        status_code: STATUS_CODES.ERROR_CODE,
      };
    }
    existingNews.isActive = true;
    existingNews.save();
    return {
      message: "successfully active",
      status_code: STATUS_CODES.SUCCESS_CODE,
    };
  } catch (e: any) {
    return { errorMessage: e, status_code: STATUS_CODES.ERROR_CODE };
  }
}

export async function newsInActiveService(id: string) {
  try {
    const existingNews = await NewsModel.findById(id);
    if (!existingNews) {
      return {
        errorMessage: "No News Found",
        status_code: STATUS_CODES.ERROR_CODE,
      };
    }
    existingNews.isActive = false;
    existingNews.save();
    return {
      message: "successfully in-active",
      status_code: STATUS_CODES.SUCCESS_CODE,
    };
  } catch (e: any) {
    return { errorMessage: e, status_code: STATUS_CODES.ERROR_CODE };
  }
}

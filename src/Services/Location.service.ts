import { DocumentDefinition } from "mongoose";
import Location, { LocationDocument } from "../Models/Location.model";
import {
  STATUS_CODES,
  SUCCESS_MESSAGE,
  SUCCESS_UPDATE_MESSAGE,
} from "../utils/constants";

export async function createLocation(
  input: DocumentDefinition<LocationDocument>
) {
  try {
    const existingLocation = await Location.find({ location: input.location });
    if (existingLocation.length) {
      return {
        errorMessage: "Location already Exists",
        errorCode: STATUS_CODES.ERROR_CODE,
      };
    }
    const result: any = await Location.create(input);
    return result;
  } catch (error: any) {
    return { errorMessage: error, errorCode: STATUS_CODES.ERROR_CODE };
  }
}

export async function editLocationService(
  input: DocumentDefinition<LocationDocument>
) {
  try {
    if (!input._id) {
      return { message: "Pass id", status_code: STATUS_CODES.ERROR_CODE };
    }
    const existingLocation = await Location.findById(input._id);
    if (!existingLocation) {
      return {
        errorMessage: "location not Exists",
        errorCode: STATUS_CODES.ERROR_CODE,
      };
    }
    Object.assign(existingLocation, input);
    existingLocation.save();
    return { message: SUCCESS_UPDATE_MESSAGE };
  } catch (error: any) {
    return { errorMessage: error, errorCode: STATUS_CODES.ERROR_CODE };
  }
}

export async function GetLocation() {
  const location = await Location.find();
  if (location.length) {
    return { message: "location fetched success", data: location };
  } else {
    return { message: "location fetched failed", data: [] };
  }
}

export async function deleteLocation(id: string) {
  try {
    const location = await Location.findByIdAndDelete(id);
    return location;
  } catch (e: any) {
    return { errorMessage: e, errorCode: STATUS_CODES.ERROR_CODE };
  }
}

export async function activeLocation(id: string) {
  try {
    const existingLocation = await Location.findById(id);
    if (!existingLocation) {
        return { message: "Cant find location", status_code: STATUS_CODES.ERROR_CODE };
    }
    existingLocation.isActive = true;
    await existingLocation.save();
    return { message: "Success", status_code: STATUS_CODES.SUCCESS_CODE };
  } catch (e: any) {
    return { errorMessage: e, errorCode: STATUS_CODES.ERROR_CODE };
  }
}

export async function inActiveLocation(id: string) {
    try {
      const existingLocation = await Location.findById(id);
      if (!existingLocation) {
          return { message: "Cant find location", status_code: STATUS_CODES.ERROR_CODE };
      }
      existingLocation.isActive = false;
      await existingLocation.save();
      return { message: "Success", status_code: STATUS_CODES.SUCCESS_CODE };
    } catch (e: any) {
      return { errorMessage: e, errorCode: STATUS_CODES.ERROR_CODE };
    }
  }

import express,  { Request, Response }  from "express";
import { createUserHandler, loginUserHandler } from '../Connectors/user.controller';
const router = express.Router();



router.post("/v1/api/user/register", (req: Request, res: Response) => {
    // return res.json({"message": "hello world"});
    createUserHandler(req, res);
});

router.post("/v1/api/user/login", (req: Request, res: Response) => {
    // return res.json({"message": "hello world"});
    loginUserHandler(req,res);
});



export { router as userRoute };
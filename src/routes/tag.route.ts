import express,  { Request, Response }  from "express";
import { createUserHandler, loginUserHandler } from '../Connectors/user.controller';
const router = express.Router();



router.get("/v1/api/news/tag", (req: Request, res: Response) => {
    createUserHandler(req, res);
});

router.post("/v1/api/news/tag", (req: Request, res: Response) => {
    loginUserHandler(req,res);
});

router.put("/v1/api/news/tag", (req: Request, res: Response) => {
    loginUserHandler(req,res);
});

router.delete("/v1/api/news/tag", (req: Request, res: Response) => {
    loginUserHandler(req,res);
});

router.get("/v1/api/news/tag/active/:id", (req: Request, res: Response) => {
    loginUserHandler(req,res);
});

router.get("/v1/api/news/tag/inactive/:id", (req: Request, res: Response) => {
    loginUserHandler(req,res);
});



export { router as userRoute };
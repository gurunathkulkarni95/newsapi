import express, { Request, Response } from "express";
import {
  getNews,
  createNews,
  createNewsTypes,
  getNewsTypes,
  editNews,
  editNewsType,
  deleteNewsType,
  deleteNews,
  newsActive,
  newsTypeActive,
  newsTypeInActive,
  newsInActive,
} from "../Connectors/news.controller";
const router = express.Router();

// NEWS API
router.post("/v1/api/news/", (req: Request, res: Response) => {
  createNews(req, res);
});

router.get("/v1/api/news/", (req: Request, res: Response) => {
  getNews(req, res);
});

router.put("/v1/api/news/", (req: Request, res: Response) => {
  editNews(req, res);
});

router.delete("/v1/api/news/:id", (req: Request, res: Response) => {
  deleteNews(req, res);
});

// active inactive Routes
router.get("/v1/api/news/active/:id", (req: Request, res: Response) => {
  newsActive(req, res);
});

router.get("/v1/api/news/inactive/:id", (req: Request, res: Response) => {
  newsInActive(req, res);
});
// End of inactive Routes

// END OF NEWS API

// NEWS TYPE ROUTES
router.post("/v1/api/news/type", (req: Request, res: Response) => {
  createNewsTypes(req, res);
});

router.get("/v1/api/news/type", (req: Request, res: Response) => {
  getNewsTypes(req, res);
});

router.put("/v1/api/news/type", (req: Request, res: Response) => {
  editNewsType(req, res);
});

router.post("/v1/api/news/type/delete", (req: Request, res: Response) => {
  deleteNewsType(req, res);
});

router.get("/v1/api/news/type/active/:id", (req: Request, res: Response) => {
  newsTypeActive(req, res);
});
router.get("/v1/api/news/type/inactive/:id", (req: Request, res: Response) => {
  newsTypeInActive(req, res);
});
// END OF NEWS TYPE ROUTES

export { router as NewsRoute };

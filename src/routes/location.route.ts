import express, { Request, Response } from "express";
import {
  activeLocationController,
  createLocationHandler,
  deleteLocationController,
  editLocation,
  inActiveLocationController,
  listLocation,
} from "../Connectors/Location.controller";
const router = express.Router();

router.get("/v1/api/news/location", (req: Request, res: Response) => {
  listLocation(req, res);
});

router.post("/v1/api/news/location", (req: Request, res: Response) => {
  createLocationHandler(req, res);
});

router.put("/v1/api/news/location", (req: Request, res: Response) => {
  editLocation(req, res);
});

router.delete("/v1/api/news/location/:id", (req: Request, res: Response) => {
  deleteLocationController(req, res);
});

router.get(
  "/v1/api/news/location/active/:id",
  (req: Request, res: Response) => {
    activeLocationController(req, res);
  }
);

router.get(
  "/v1/api/news/location/inactive/:id",
  (req: Request, res: Response) => {
    inActiveLocationController(req, res);
  }
);

export { router as locationRoute };

// importing mongoDb
import mongoose from "mongoose";

function connect() {
  const databaseName = "NewsAppDb";
  const remoteURL = `mongodb+srv://dukandatabase:dukandatabase@dukandatabase.dpwex.mongodb.net/${databaseName}?retryWrites=true&w=majority`;

  // const connectionUrl = `mongodb://127.0.0.1:27017/${databaseName}`;

  mongoose
    .connect(remoteURL)
    .then(() => {
      console.log("Connected to Database...!");
    })
    .catch((err: any) => {
      console.log(err);
    });
}

export default connect;

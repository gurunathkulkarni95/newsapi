import mongoose from "mongoose";

export interface NewsDocument extends mongoose.Document {
    newsTitle: string,
    newsDescription: string,
    image: imageObject,
    bannerImage: string,
    isActive: boolean,
    tag: any,
    typeOfNews: any,
    reporterName: string,
    location: string
}

interface imageObject {
  para_id: string,
  image_url: string,
  position: string
}

const NewsSchema = new mongoose.Schema(
  {
    newsTitle: { type: String, required: true, unique: true },
    newsDescription: { type: String, required: true },
    image: { type: [], required: false },
    isActive: { type: Boolean, required: false },
    tag: { type: [], required: false },
    typeOfNews: { type: [], required: false },
    reporterName: { type: String, required: false },
    location: { type: String, required: false }
  },
  { timestamps: true }
);

const NewsModel = mongoose.model<NewsDocument>("News", NewsSchema);

export default NewsModel;
import mongoose from "mongoose";

export interface TagDocument extends mongoose.Document {
  tagName: string,
  isActive: boolean,
  createdAt: Date;
  updatedAt: Date;
}

const TagSchema = new mongoose.Schema(
  {
    tagName: { type: String, required: true, unique: true },
    isActive: { type: Boolean, required: true, default: true}
  },
  { timestamps: true }
);

const Tag = mongoose.model<TagDocument>("Location", TagSchema);

export default Tag;

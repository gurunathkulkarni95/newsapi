import mongoose from "mongoose";

export interface LocationDocument extends mongoose.Document {
  location: string,
  isActive: boolean,
  createdAt: Date;
  updatedAt: Date;
}

const LocationSchema = new mongoose.Schema(
  {
    location: { type: String, required: true, unique: true },
    isActive: { type: Boolean, required: true, default: true}
  },
  { timestamps: true }
);

const Location = mongoose.model<LocationDocument>("Location", LocationSchema);

export default Location;

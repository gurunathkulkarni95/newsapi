import mongoose from "mongoose";

export interface NewstypeDocument extends mongoose.Document {
    type: string,
    type_en: string,
    type_kn: string,
    type_hi: string,
    type_mr: string,
    type_bn: string,
    isActive: boolean
}

const NewsTypeSchema = new mongoose.Schema(
  {

    type: { type: String, required: true },
    type_en: { type: String, required: true },
    type_kn: { type: String, required: false },
    type_hi: { type: String, required: false },
    type_mr: { type: String, required: false },
    type_bn: { type: String, required: false },
    isActive: { type: Boolean, required: true, default: true }
  },
  { timestamps: true }
);

const NewsTypeModel = mongoose.model<NewstypeDocument>("NewsType", NewsTypeSchema);

export default NewsTypeModel;
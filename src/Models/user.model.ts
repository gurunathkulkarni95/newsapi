import mongoose from "mongoose";

export interface UserDocument extends mongoose.Document {
  email: string;
  firstname: string;
  lastname: string;
  dob: string;
  mobileNumber: string;
  type: string;
  password: string;
  createdAt: Date;
  updatedAt: Date;
  comparePassword(candidatePassword: string): Promise<boolean>;
}

const UserSchema = new mongoose.Schema(
  {
    email: { type: String, required: true, unique: true },
    firstname: { type: String, required: true },
    lastname: { type: String, required: false },
    dob: { type: String, required: false },
    mobileNumber: { type: String, required: true },
    type: { type: String, required: true },
    password: { type: String, required: true },
  },
  { timestamps: true }
);

const User = mongoose.model<UserDocument>("User", UserSchema);

export default User;
